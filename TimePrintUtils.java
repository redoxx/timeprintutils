package com.test;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class TimePrintUtils {

    private TimePrintUtils() {}

    /**
     * The final variable for storing the tasks and start timings
     */
    final private static ConcurrentMap<String, LocalDateTime> timeMap = new ConcurrentHashMap<>();

    /**
     * This marks the starting of the time capture for a particular task
     * @param task: String
     * @return startTime: LocalDateTime
     */
    public static LocalDateTime start(String task) {
        if (Objects.nonNull(timeMap.get(task))) {
            throw new IllegalStateException("TimePrintUtils is already initialized with the given task!");
        }
        timeMap.put(task, LocalDateTime.now(Clock.systemUTC()));
        return timeMap.get(task);
    }

    /**
     * This marks the ending of the time capture for a particular task
     * @param task: String
     * @return endTime: LocalDateTime
     */
    public static LocalDateTime stop(String task) {
        LocalDateTime out = LocalDateTime.now(Clock.systemUTC());
        LocalDateTime in = timeMap.get(task);
        if (Objects.isNull(in)) {
            throw new IllegalStateException("TimePrintUtils is not initialized with the given task!");
        }
        System.out.println("The task '" + task + "' took " + (out.getNano() - in.getNano()) + "ns");
        TimePrintUtils.remove(task);
        return out;
    }

    /**
     * This resets the time capture for a given task
     * @param task: String
     */
    public static void reset(String task) {
        if (Objects.isNull(timeMap.get(task))) {
            throw new IllegalStateException("TimePrintUtils is not initialized with the given task!");
        }
        TimePrintUtils.remove(task);

    }

    /**
     * This removes the time information for a given task from the data storage
     * @param task: String
     */
    private static void remove(String task) {
        timeMap.remove(task);
    }

    /**
     * This resets the complete data storage
     */
    public static void resetAll() {
        timeMap.clear();
    }
}
